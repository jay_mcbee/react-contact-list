var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var config = {
  entry: APP_DIR + '/root.js',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
	module : {
	loaders : [
	  {
      test: /\.js?$/,
      loader: 'babel',
      exclude: /node_modules/,
      query: {
        presets: ['react', 'es2015', 'stage-2'],
        plugins: ["transform-object-rest-spread"]
      }
    },
    { test: /\.css$/, loader: "style-loader!css-loader" }        
	]
}
}

module.exports = config;


