var express = require('Express');
var bodyParser = require('body-parser')
var app = express();
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var webpack = require('webpack');
var config = require('../webpack.config');
var compiler = webpack(config);

//allows webpack rebuilds on server restarts
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
app.use(webpackHotMiddleware(compiler));



app.use( bodyParser.urlencoded ({extended:true}) );
app.use(bodyParser.json());

app.use(express.static( '../src'));

require('./routes')(app);


var port = process.env.PORT || 3000;

app.listen(port);

console.log('Server is up on port:' + port);