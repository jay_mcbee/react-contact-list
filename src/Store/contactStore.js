var AppDispatcher = require('../dispatcher/AppDispatcher');
var appConstants = require('../constants/appConstants');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';


//holds current state of app
var _store = {
  contacts: [],
  fetching: false,
  search: null,
  searchName: '',
  editing: false,
  contactToEdit: null,
  newContact: {name:'', email:''}
};

//searches _store.contacts for contact with matching name
var findContact = function(name){
  var person = _store.contacts.filter( conObj => conObj.name === name || conObj.name.toLowerCase() === name)[0];
  if(person) return person;
  else return 'Contact is not registered';
}

//searches _store.contacts for contact with matching id
var findEdit = function(id){
  var selectContact = _store.contacts.filter( conObj => conObj.id === id)[0];
  if(selectContact) return selectContact;
  else return 'Contact is not registered';
}

//only export from module--used to connect container to state--provides state listeners and current state
var contactStore = Object.assign({}, EventEmitter.prototype, {
  addChangeListener: function(cb){
    this.on(CHANGE_EVENT, cb);
  },
  removeChangeListener: function(cb){
    this.removeListener(CHANGE_EVENT, cb);
  },
  getState: function(){
    return _store;
  }
});

//switch statement that handles action types and alters state appropriately
AppDispatcher.register(function(payload){
  var action = payload.action;
  switch(action.actionType){
    case appConstants.ADD_SEARCH:
      _store.searchName = action.data;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.ADD_NAME_C:
      _store.newContact.name = action.data;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.ADD_EMAIL_C:
      _store.newContact.email = action.data;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.ADD_NAME:
      _store.contactToEdit.name = action.data;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.ADD_EMAIL:
      _store.contactToEdit.email = action.data;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.GET_CONTACT:
      _store.fetching = true;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.ADD_CONTACT:
      _store.fetching = true;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.FIND_CONTACT:
      _store.searchName = '';
      _store.search = findContact(action.data);
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.REMOVE_CONTACT:
      _store.fetching = true;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.CLOSE_SEARCH:
      _store.search = null;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.EDIT_CONTACT:
      _store.editing = true;
      _store.contactToEdit = findEdit(action.data);
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.SEND_EDITS:
      _store.contactToEdit.name = action.data.name;
      _store.contactToEdit.email = action.data.email;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.SEND_CONTACT:
      _store.editing = false;
      _store.contactToEdit = '';
      _store.fetching = true;
      contactStore.emit(CHANGE_EVENT);
      break;
    case appConstants.GET_RESP:
      _store = Object.assign( {}, 
          _store ,{
            fetching: false,
            editing: false,
            contacts: action.response.data,
            newContact: {name: '', email: ''}
          })
      contactStore.emit(CHANGE_EVENT);
      break;
    default:
      return true;
  }
});
module.exports = contactStore;
