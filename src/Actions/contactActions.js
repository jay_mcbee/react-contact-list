var AppDispatcher = require('../dispatcher/AppDispatcher');
var appConstants = require('../constants/appConstants');
var ContactsAPI = require('../util/ContactsAPI');


var contactActions = {
 
  //action associated with input elements
  addCharToName: function(char){
    AppDispatcher.handleAction({
      actionType:appConstants.ADD_NAME_C,
      data: char
    })
  },

  addCharToEmail: function(char){
    AppDispatcher.handleAction({
      actionType:appConstants.ADD_EMAIL_C,
      data: char
    })
  },
  addSearchChar: function(char){
    AppDispatcher.handleAction({
      actionType:appConstants.ADD_SEARCH,
      data: char
    })
  },

  addNameChar: function(char){
    AppDispatcher.handleAction({
      actionType:appConstants.ADD_NAME,
      data: char
    })
  },

  addEmailChar: function(char){
    AppDispatcher.handleAction({
      actionType:appConstants.ADD_EMAIL,
      data: char
    })
  },

  //ContactsAPI methods call async req to server
  
  getContact: function(){
    AppDispatcher.handleAction({
      actionType: appConstants.GET_CONTACT
    })
    ContactsAPI.get();
  },
  
  /*item in following lines--22,28,34
  input values from add contact(contactForm.jsx) 
  and edit contact(editContact.jsx)>*/

  
  addContact: function(item){
    AppDispatcher.handleAction({
      actionType: appConstants.ADD_CONTACT
    });
    ContactsAPI.post(item);
  },
  editContact: function(item){
    AppDispatcher.handleAction({
      actionType: appConstants.EDIT_CONTACT,
      data: item
    })
  },
  sendContact: function(item){
    AppDispatcher.handleAction({
      actionType: appConstants.SEND_CONTACT
    })
    ContactsAPI.put(item);
  },
  //name entered into search bar
  findContact: function(name){
    AppDispatcher.handleAction({
      actionType: appConstants.FIND_CONTACT,
      data: name
    })
  },
  closeContact: function(){
    AppDispatcher.handleAction({
      actionType: appConstants.CLOSE_SEARCH
    })
  },
  //item-> id of contact in db
  removeContact: function(item){
    AppDispatcher.handleAction({
      actionType: appConstants.REMOVE_CONTACT
    })
    ContactsAPI.delete(item)
  }
};
module.exports = contactActions;
