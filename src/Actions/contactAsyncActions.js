var AppDispatcher = require('../dispatcher/AppDispatcher');
var appConstants = require('../constants/appConstants');


//dispatches response to store from async req
module.exports = {

	receiveResp: function(response){
		AppDispatcher.handleServerAction({
			actionType: appConstants.GET_RESP,
			response: response
		});
	}
}