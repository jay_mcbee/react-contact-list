var asyncActions = require('../actions/contactAsyncActions');
var axios = require('axios');

module.exports = {

  //all async requests to contactsAPI
  //axios requests are all promisified
  

  //req for all contacts
  get: function() {
    axios.get('/contacts')
      .then( res => asyncActions.receiveResp(res));
  },
  //req to create contact in db
  post: function(formData) {
    axios.post('/contacts',formData)
      .then( res => asyncActions.receiveResp(res))
  },
  //req to edit contact in db
  put: function(formData) {
  	axios.put('/contacts', formData)
      .then( res => asyncActions.receiveResp(res))
  },
  //req to remove contact from db
  delete: function(id) {
  	axios.delete('/contacts', {params: {id: id}})
      .then( res => asyncActions.receiveResp(res))
  }
};