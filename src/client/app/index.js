import React, { Component, PropTypes} from 'react';
import { render } from 'react-dom';
import Contacts from '../Components/contacts';
import ContactForm from '../Components/contactForm';
import SearchBar from '../Components/searchBar';
import contactStore from '../../Store/contactStore.js';
import contactActions from '../../Actions/contactActions.js';
import '../styles.css';

export default class App extends Component {
  constructor(){
    super();
    this._onChange = this._onChange.bind(this);
    this.state = contactStore.getState();
  }

  componentWillMount(){
    contactActions.getContact();
  }

  componentDidMount(){
    contactStore.addChangeListener(this._onChange);
  };

  componentWillUnmount() {
    contactStore.removeChangeListener(this._onChange);
  };

  _onChange() {
    this.setState(contactStore.getState());
  }

  render () {

    return(
      <div className='container pad'>
        <div className = 'row'>
          <div className='col-md-3'></div>
            <div className='col-md-6 border pad'>
          	  <div className='col-md-6'>
                <SearchBar
                  search = {this.state.search}
                  searchName = {this.state.searchName}
                />
                <ContactForm
                  newContact = {this.state.newContact}
                />
              </div>
              <div className='col-md-6'>
                <Contacts
                  contacts = {this.state.contacts}
                  editing = {this.state.editing}
                  contactToEdit = {this.state.contactToEdit}
                />
              </div>
            </div>
          <div className='col-md-3'></div>
        </div>
      </div>
    )
  }
}

App.PropTypes = {
  search : PropTypes.object.isRequired,
  searchName: PropTypes.string.isRequired,
  newContact: PropTypes.object.isRequired,
  contacts: PropTypes.array.isRequired,
  editing: PropTypes.bool.isRequired,
  contactToEdit: PropTypes.object.isRequired
}
