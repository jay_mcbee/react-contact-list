import {Router, Route, browserHistory} from 'react-router';
import React, {Component} from 'react';
import {render} from 'react-dom';
import App from './index';


//router is not necessary for such a simple app
//used here to show understanding

class Root extends Component{
	render(){
		return(
		  <Router history={browserHistory}>
		    <Route path='/' component={App}/>
		  </Router>
		)
	}
}

render(<Root/>, document.getElementById('app'));


