import React from 'react';
import contactActions from '../../Actions/contactActions'

export default function ContactMaker(contactObj){

	let [remove, edit] = [contactActions.removeContact, contactActions.editContact];

	return(
	  <div className='fiveMargin border pad' key={contactObj.id}>
	    <div>
	      <i 
	        className = 'glyphicon glyphicon-remove-circle pull-right alertIcon point' 
	        onClick = {() => remove(contactObj.id)}>
	      </i>
	      <small>Name: {contactObj.name}</small>
	    </div>
	    <div>
	      <i 
	        className = 'glyphicon glyphicon-pencil pull-right point' 
	        onClick = {() => edit(contactObj.id)}>
	      </i>
	      <small>Email: {contactObj.email}</small>
	    </div>
	  </div>
	)
}