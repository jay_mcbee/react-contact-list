import React, {Component, PropTypes} from 'react';
import {render} from 'react-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import contactActions from '../../Actions/contactActions.js'


export default class EditContact extends Component{
   
  handleEmailChange(event){
    contactActions.addEmailChar(event.target.value);
  };

  handleNameChange(event){
    contactActions.addNameChar(event.target.value);
  };
  
  //dispatches action to store and also sends async req to update contact in db
  handleSubmit(obj){
  	contactActions.sendContact(obj);
  }
  
  render() {
    return (
      <ReactCSSTransitionGroup 
        transitionName={'contact'} 
        transitionAppear={true}  
        transitionAppearTimeout={500} 
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}>
        <div className='text-center pad'>
        <br />
          <input 
            type='text' 
            placeholder={this.props.contactToEdit.name} 
            onChange={this.handleNameChange.bind(this)}/>
        <br />
          <input 
            type='text' 
            placeholder={this.props.contactToEdit.email} 
            onChange={this.handleEmailChange.bind(this)}/>
        <br />
          <button 
            className='btn btn-primary btn-block fiveMargin'
            onClick={() => this.handleSubmit(this.props.contactToEdit)}> 

            Edit Contact
          </button>
        </div>
      </ReactCSSTransitionGroup>  
    )
  }
};

EditContact.PropTypes = {
  contactToEdit : PropTypes.object.isRequired
}