import React, {Component, PropTypes} from 'react';
import {render} from 'react-dom';
import SearchResult from '../Components/searchResult';
import contactActions from '../../Actions/contactActions.js'


export default class SearchBar extends Component{
  
  handleNameChange(event){
    contactActions.addSearchChar(event.target.value);
  };
  
  //obj->value submitted from search bar
  handleSubmit(obj){
    //resets input value to empty string
    document.getElementById('search').value = '';
    //sends action to store that searches through contacts array for match
  	contactActions.findContact(obj);
  }
  
  render() {
    
    let {closeContact} = contactActions; 
    
    return (
      <div>
        <div className='text-center fiveMargin'>
          <input 
            id='search' 
            placeholder='name' 
            type='text' 
            onChange={this.handleNameChange.bind(this)}/>
          <br />
          <button 
            className='btn btn-primary btn-block fiveMargin' 
            onClick={ () => this.handleSubmit(this.props.searchName)}>
            Search
          </button>
        </div>
        <div>
         {this.props.search !== null ?
          <SearchResult
            name = {this.props.search.name}
            email = {this.props.search.email}
            close = {closeContact}
          />
          :null}
        </div>
      </div>
    )
  }
};

SearchBar.PropTypes = {
  search: PropTypes.object.isRequired
}