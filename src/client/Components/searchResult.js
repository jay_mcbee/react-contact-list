import React, {Component, PropTypes} from 'react';
import {render} from 'react-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';




export default class SearchResult extends Component{

	render(){
		return(
      <ReactCSSTransitionGroup 
        transitionName={'contact'} 
        transitionAppear={true}  
        transitionAppearTimeout={500} 
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}>
  			<div className='border pad'>
          <i 
            className = 'pull-right alertIcon point' 
            onClick = { () => this.props.close()}>
            &#10006;
          </i>
          <small>Name: {this.props.name}<br />Email: {this.props.email}</small>
        </div>
      </ReactCSSTransitionGroup>  		
    )
	}
}

SearchResult.PropTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
}