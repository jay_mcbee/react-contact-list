import React, { Component, PropTypes } from 'react';
import { render } from 'react-dom';
import EditContact from '../Components/editContact';
import ContactMaker from './contactMaker';

export default class Contacts extends Component {
  

  render () {

    //contactElements-->results array of JSX elements from mapping over contactObjects
    //ContactMaker func-->contactMaker.js
    //ES6 spread operator expands the array of contact JSX elements in the return call of the render function ln. 48
  	
    let contactElements = this.props.contacts.map( contactObj => ContactMaker(contactObj));
    
    return (
      <div>
        <div className='text-center border pad'>
          <h2>Contacts</h2>
          {this.props.editing ? 
            <EditContact
              contactToEdit = {this.props.contactToEdit}
            /> 
          : null}          
          <div className='text-left'>
            {[...contactElements]}
          </div>
        </div>
      </div>
    )
  }
}

Contacts.PropTypes = {
  editing: PropTypes.bool.isRequired,
  contactToEdit: PropTypes.object.isRequired

}