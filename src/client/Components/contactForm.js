import React, {Component, PropTypes} from 'react';
import {render} from 'react-dom';
import contactActions from '../../Actions/contactActions.js'


export default class ContactForm extends Component{

  handleEmailChange(event){
    contactActions.addCharToEmail(event.target.value);
  };

  handleNameChange(event){
    contactActions.addCharToName(event.target.value);
  };

  handleSubmit(obj){
    //reset inputs to empty string
    document.getElementById('name').value = '';
    document.getElementById('email').value = '';
    contactActions.addContact(obj);
  }
  
  render() {
    return (
      <div className='text-center fiveMargin'>
        Add Contact
        <br />
        <input 
          id='name' 
          type='text' 
          placeholder='name'
          onChange= {this.handleNameChange.bind(this)}/> 
        <br />
        <input 
          id='email' 
          type='text' 
          placeholder='email'
          onChange = {this.handleEmailChange.bind(this)}/> 
        <br />
        <button 
          className='btn btn-primary btn-block fiveMargin' 
          disabled={this.props.newContact.name === ''}
          onClick={()=>this.handleSubmit(this.props.newContact)}>
          Save Contact
        </button>
      </div>
    )
  }
};

ContactForm.PropTypes = {
  newContact: PropTypes.object.isRequired
}