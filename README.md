# README #

Stay in Touch

### What is this repository for? ###

Contacts App--NERC Stack--Node/Express/React/Cassandra

### How do I get set up? ###

In terminal window execute -> cassandra -f (starts a local cluster)

Open a new terminal tab and CD into the project directory

->npm install
->npm start


Should start up at localhost:3000

### Contribution guidelines ###

You don't want to contribute to this

### Who do I talk to? ###

Question or concerns--jmcbee1@gmail.com